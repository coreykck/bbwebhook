<?php

require_once 'vendor/autoload.php';

$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
$dispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher();

// Todo: add listener to event dispatcher.
/*
$listener = ...

$dispatcher->addListener(\Smalot\Bitbucket\Webhook\Events::WEBHOOK_REQUEST, $listener);
*/
$webhook = new \Smalot\Bitbucket\Webhook\Webhook($dispatcher);
$event = $webhook->parseRequest($request);
$payload = json_decode($event->getPayload());

$data = [
  'repo' => $payload->repository->full_name,
  'branch' => $payload->push->changes[0]->new->name,
];


$value = \Symfony\Component\Yaml\Yaml::parse(file_get_contents('config/config.yml'));
$repos = $value['deploy']['repos'];
$branches = $value['deploy']['definitions'];
$config_file = $value['deploy']['config_file'];

foreach ($repos as $repo) {
  if ($repo['full_name'] == $data['repo']) {
    $params = [
      $config_file,
      $repo['name'],
      $branches[$data['branch']],
    ];
    $tconfig_file = call_user_func_array('sprintf', $params );

    $fs = new \Symfony\Component\Filesystem\Filesystem();
    $fs->appendToFile('./todeploy/queue.inc', $tconfig_file . ' ' . $repo['type'] . PHP_EOL);
    die( "Deploy queued" . PHP_EOL );
  }
}

header("Status: 404 Not Found");
die( "Deploy configuration NOT FOUND" . PHP_EOL );